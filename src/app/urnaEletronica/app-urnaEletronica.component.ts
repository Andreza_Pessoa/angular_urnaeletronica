import { UrnaService } from './../service/urna.service';
import { Component, EventEmitter, Output } from "@angular/core";


@Component({
    selector: "app-urnaEletronica",
    templateUrl: "./app-urnaEletronica.component.html",
    styleUrls: ["./app-urnaEletronica.component.scss"]
})
export class AppUrnaEletronica{

   // @Output() aoVotar = new EventEmitter<any>()

    rg : number = 0;
    numeroCandidato: number = 0;
    listaCandidato : string = ""

    //recebe urnaService
      constructor(private service : UrnaService){

      }
    public confirmar(){
        console.log("RG: " + this.rg);
        console.log("numero do candiato: " + this.numeroCandidato);
        console.log("candidato: " + this.listaCandidato);

        const voto = {
                            rg: this.rg,
                            numeroCandidato :this.   numeroCandidato,
                             listaCandidato: this.listaCandidato }

       // this.aoVotar.emit(voto)
      // this.service.addUrna(voto)

      this.service.addUrna(voto).subscribe(resultado => {
        console.log(resultado);
        this.limparTela()
      },error =>{
        console.error(error)
      })
    }

    //limpar dados da tela
    private limparTela(){
        this.rg = 0;
        this.numeroCandidato = 0
        this.listaCandidato = ""
    }

    public branco(){
      console.log("voto em branco");

    }
}
