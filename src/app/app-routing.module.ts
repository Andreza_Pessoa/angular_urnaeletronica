import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { RelatorioComponent } from "./relatorio/relatorio.component";
import { AppUrnaEletronica } from "./urnaEletronica/app-urnaEletronica.component";

export const routes : Routes = [
  {path: "", redirectTo: "relatorio", pathMatch: "full"},
  {path: "relatorio", component: RelatorioComponent },
  {path: "urnaEletronica", component:  AppUrnaEletronica }
]

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule{

}
