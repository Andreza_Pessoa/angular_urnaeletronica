import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { UrnaEletronica } from 'models/urnaEletronica.model';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class UrnaService {

  private _listaUrna: any[] = [];
  private url = "http://localhost:3000/urnaEletronica";

constructor(private httpClient: HttpClient) { }


public getAllUrna(): Observable<UrnaEletronica[]>{
  return this.httpClient.get<UrnaEletronica[]>(this.url)
}
 public addUrna(voto : any) : Observable<UrnaEletronica> {

      this.carimbar(voto)
      //this._listaUrna.push(voto)
      return  this.httpClient.post<UrnaEletronica>(this.url,voto)
  }

  private carimbar(voto : any){
    voto.data = new Date().toLocaleString()
    //vai a regra de negocio(limite de horario para voto, etc)

    // let now = new Date
    // if (now.getHours () >= 0 || now.getHours() <24) {
    // document.write("Fora do horario  de votação")
    // }
  }

  public get listaUrna(): any[] {
    return this._listaUrna;
  }

  public set listaUrna(value: any[]) {
    this._listaUrna = value;
  }

}
