/* tslint:disable:no-unused-variable */

import { TestBed, async, inject } from '@angular/core/testing';
import { UrnaService } from './urna.service';

describe('Service: Urna', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [UrnaService]
    });
  });

  it('should ...', inject([UrnaService], (service: UrnaService) => {
    expect(service).toBeTruthy();
  }));
});
