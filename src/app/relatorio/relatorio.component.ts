import { UrnaService } from './../service/urna.service';
import { Component, Input, OnInit } from '@angular/core';
import { UrnaEletronica } from 'models/urnaEletronica.model';

@Component({
  selector: 'app-relatorio',
  templateUrl: './relatorio.component.html',
  styleUrls: ['./relatorio.component.scss']
})
export class RelatorioComponent implements OnInit {

  // @Input() votos : any[] = [];
  votos : any[] = [];

  constructor(private service : UrnaService) { }

  ngOnInit(): void {

   // this.votos = this.service.listaUrna

   this.service.getAllUrna().subscribe((urnaPP: UrnaEletronica[]) => {
      console.table(urnaPP)
      this.votos = urnaPP
   })
  }

}
