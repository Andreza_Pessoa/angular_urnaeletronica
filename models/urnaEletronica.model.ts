

  export interface UrnaEletronica {
      id: number;
      rg: number;
      numeroCandidato: number;
      listaCandidato: string;
      data: Date;
  }

